import express, {Response, Request} from "express";
import cors from "cors";
import bodyParser from "body-parser";
import {Server} from 'typescript-rest';

import "./routes"


export const app:express.Application= express();

app.use(cors());
app.use(bodyParser.json());

Server.buildServices(app);

let port = parseInt(process.env.PORT || "");

if(isNaN(port) || port === 0){
    port = 4000;
}

app.listen(port, "0.0.0.0", () => {
    console.log(`Bismillah, server is running on ${port}`);
});
